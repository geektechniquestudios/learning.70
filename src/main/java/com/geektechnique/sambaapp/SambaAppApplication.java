package com.geektechnique.sambaapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SambaAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(SambaAppApplication.class, args);
    }

}
