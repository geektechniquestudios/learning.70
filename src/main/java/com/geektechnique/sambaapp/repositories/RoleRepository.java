package com.geektechnique.sambaapp.repositories;

import com.geektechnique.sambaapp.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository  extends JpaRepository<Role, String> {
 
}
